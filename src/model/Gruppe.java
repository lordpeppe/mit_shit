package model;

import java.util.ArrayList;

public class Gruppe {
	private ArrayList<Person> gruppe = new ArrayList<Person>();
	private String navn;

	public Gruppe(String navn) {
		this.navn = navn;
	}

	public String getNavn() {
		return navn;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

}
